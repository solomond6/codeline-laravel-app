<div class="card-body">
  {!! Form::open(['url'=>'new', 'class'=>'form']) !!}
    <div class="form-group has-feedback">
      {!! Form::label('User Name') !!}
      {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group has-feedback">
      {!! Form::label('User Email') !!}
      {!! Form::email('email', null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::submit('Submit', ['class'=>'btn btn-primary btn-block btn-flat']) !!}
    </div>
  {!! Form::close() !!}
</div>