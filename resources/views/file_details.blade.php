@foreach ($films as $film)
    <div class="col-md-12">
        <a class="banner banner-1 overlay black" href="{{ url('films/'.$film->slug) }}">
            <img src="/images/products/small/{{$film->image_path}}"/>
            <div class="banner-caption text-center">
                <h2>{{$film->name}}</h2>
                <p>{{$film->description}}</p>
                <p>{{$film->release_date}}</p>
                <p>{{$film->amount}}</p>
            </div>
        </a>
    </div>
@endforeach