<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Films;

class FilmsController extends Controller
{
    public function getAllFilms(){
    	try{
            $films = Films::all();

            if($films== null){
                return response()->json(null, 204);
            }else{
                return response()->json($films, 200);
            }
        }catch(\Exception $e){
            echo json_encode(['Count'=> '0','Message'=>'Bad Request', 'Results'=>[]]);
        }
    	
    }

    public function getFilm($slug){
    	try{
        	$film = Films::where('slug', '=', $slug)->first();
        	
            if($film==null){
                return response()->json(null, 204);
            }else{
            	return response()->json($film, 200);
            }
        }catch(\Exception $e){
            echo json_encode(['Count'=> '0','Message'=>'Bad Request', 'Results'=>[]]);
        }
    }
}
