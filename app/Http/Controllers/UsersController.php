<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
	public function index(){
    	return view('users');
    }

    public function adduser(Request $request){
    	
    	if($request->isMethod('post')){
    		$data = $request->all();
    		
    		$user = User::create([
	          'name'     => $data['name'],
	          'email'    => $data['email'],
	          'password' => bcrypt('secret'),
	        ]);

    		if($user){
				return redirect('/users')->with('success', 'User Created Successfully');
    		}else{
				return redirect('/users/new')->with('error', 'Error occcured when creating user, kindly try again.');
    		}
    	}
    	
    	return view('add_user'));
    }
}
