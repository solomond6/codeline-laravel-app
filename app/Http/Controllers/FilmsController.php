<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class FilmsController extends Controller
{
    public function getAllFilms(Request $request){
        
        try{
            $client = new Client();
                
            $request = $client->get('http://localhost:8000/api/getFilms');
            //Get the actual response without headers
            $films = $request->getBody();
            //var_dump($response);exit;
            return view('films')->with(compact('films'));
        }
        catch (RequestException $e) {

        	$response = "Bad Request";
            return view('films')->with(compact('response'));

        } catch (\Exception $e) {
            $response = "Bad Request";
            return view('films')->with(compact('response'));
        }
    }

    public function getFilmDetails(Request $request, $slug==null){
        
        try{
            $client = new Client();
                
            $request = $client->get('http://localhost:8000/api/getFilm'+$slug);
            //Get the actual response without headers
            $film = $request->getBody();
            //var_dump($response);exit;
            return view('film_details')->with(compact('film'));
        }
        catch (RequestException $e) {

            $response = "Bad Request";
            return view('film_details')->with(compact('response'));

        } catch (\Exception $e) {
            $response = "Bad Request";
            return view('film_details')->with(compact('response'));
        }
    }
}
