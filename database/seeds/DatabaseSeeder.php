<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genre')->insert([
            'name' => str_random(6)
        ]);

        DB::table('countries')->insert([
            'name' => str_random(10),
            'country_code'=> str_random(3)
        ]);

        DB::table('films')->insert([
            'name' => "Deceptions",
            'slug'=> "deceptions",
            'description'=> str_random(150),
            'release_date'=> "2018-09-18",
            'genre_id'=> 1,
            'country_id'=> 1,
            'votes'=> 1,
            'amount'=> 1200.00,
            'image_path'=> "",
        ]);
    }
}
